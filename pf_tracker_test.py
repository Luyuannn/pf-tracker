import cv2
import numpy as np
import czifile

class pf_tracker():
    def __init__(self, particle_num, std_move, std_r, shape, initial_midpoint):
        self.std_move = std_move
        self.std_r = std_r
        self.shape = shape
        self.particles = []
        self.weights = []
        self.particle_num = particle_num
        self.init_weights()
        self.init_particles()
        self.last_midpoint = None
        self.initial_midpoint = inital_midpoint
        self.speed = []
        
    def init_weights(self):
        self.weights = np.array([1/self.particle_num]*self.particle_num)
        
    def init_particles(self):
        self.particles = np.array([np.random.uniform(0, self.shape[i], self.particle_num) for i in range(2)]).T
        
    def get_x_given_z(self, img):
        if self.inital_midpoint == None:
            pred_mp = self.last_midpoint + self.speed
        else:
            pred_mp = self.initial_midpoint
        self.inital_midpoint = None
        
        n = 0
        crop_size = 25
        while (n < 20):
            n += 1
            crop = img[pred_mp[0]-crop_size:pred_mp[0]+crop_size, pred_mp[1]-crop_size:pred_mp[1]+crop_size]
            kernel = np.ones((5, 5), np.uint8) 
            img = cv2.dilate(img,kernel,iterations = 1)
            
            img = cv2.GaussianBlur(img,(5,5),0)
            
            ret,img = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
            
            contours, h = cv2.findContours(img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            
            if len(contours) != 2:
                if len(contours > 2):
                    crop_size -= 3
                else:
                    crop_size += 3
                    
            else:
                centres = []
                for c in contours:
                    # calculate moments for each contour
                    M = cv2.moments(c)
                    # calculate x,y coordinate of center
                    cX = int(M["m10"] / M["m00"])
                    cY = int(M["m01"] / M["m00"])
                    centres.append((cX, cY))
                    
            return np.mean(np.array(centres), 0)
        print("failed to locate midpoint, assuming constant velocity")
        return pred_mp
    
    def move_particles(self):
        for p in self.particles:
            p[:2] += np.array([p[2], p[3]]) + np.random.normal(scale = self.std_move)
            
    def update_speed(self):
        if self.last_midpoint == None:
            return
        for p in self.particles:
            p[2:] = p[:2] - self.last_midpoint
            
    def get_point_estimate(self):
        estimate = np.matmul(self.weights, self.particles[:2])
        return estimate
    
    def update_weights(self, mp):
        self.weights = np.array([np.exp(-((p[0]-mp[0])**2 + (p[1]-mp[1])**2)/2/self.std_r**2) for p in self.particles])
        self.weights /= np.sum(self.weights)
        
    def resample(self):
        minW = np.max(self.weights) / 3
        for i in range(len(self.weights)):
            if self.weights[i] < minW:
                x = np.random.normal(self.midpoint[0], self.std_r)
                y = np.random.normal(self.midpoint[1], self.std_r)
                self.particles[i][:2] = [x, y]
                
    def update(self, img):
        mp = self.get_x_given_z(img)
        self.move_particles()
        self.update_weight(mp)
        mp = self.get_point_estimate()
        self.update_speed()
        self.speed = mp - last_midpoint
        self.last_midpoint = mp
        self.resample()
        return mp
    
img = czifile.imread("worm.czi")
img = np.squeeze(img)

img = img/256 - 255/256
img = np.rint(img)
img = np.array(img, dtype=np.uint8)

for i in range img.shape[0]:

    