import cv2
import numpy as np

img = cv2.imread("worm.png", cv2.IMREAD_GRAYSCALE)
kernel = np.ones((5, 5), np.uint8) 
img = cv2.dilate(img,kernel,iterations = 1)

img = cv2.GaussianBlur(img,(5,5),0)

ret,img = cv2.threshold(img,127,255,cv2.THRESH_BINARY)

contours, h = cv2.findContours(img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

if len(contours) != 2:
    print("shit")
    
else:
    centres = []
    for c in contours:
        # calculate moments for each contour
        M = cv2.moments(c)
        # calculate x,y coordinate of center
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        centres.append((cX, cY))